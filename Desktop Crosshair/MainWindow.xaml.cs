﻿using Desktop_Crosshair.Classes;
using Microsoft.Win32;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Desktop_Crosshair
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow: Window , IOnCrosshairWindowListener
	{
		//////////////////////////////////////////////////
		#region // Member Variables

		private const string IMG_FILE = "Image Files (*.bmp;*.png;*.gif) | *.bmp;*.png;*.gif";
		private const string BMP_FILE = "Bitmap Files (*.bmp) | *.bmp";
		private const string PNG_FILE = "Portable Network Graphics Files (*.png) | *.png";
		private const string GIF_FILE = "Graphics Interchange Format Files (*.gif) | *.gif";
		private const string ALL_FILE = "All Files (*.*) | *.*";

		private const int MAX_PREVIEW_SIZE = 200;

		private Uri mCrosshairUri = null;
		private int mOffsetX = 0;
		private int mOffsetY = 0;
		private double mOpacity = 1.0;

		private bool bIsControllingOffsetWithMouse = false;
		private bool bIsOffsettingX = false;
		private bool bIsOffsettingY = false;
		private Point mPreviousPosition;

		private CrosshairWindow mCrosshairWindow = null;
		private bool bIsCrosshairEnabled = false;
		private bool bIsTaskBarVisible = true;
		
		#endregion



		//////////////////////////////////////////////////
		#region // Contructor & Helper Methods

		public MainWindow()
		{
			InitializeComponent();

			initializeMemberVariable();

			WindowStartupLocation = WindowStartupLocation.CenterScreen;
		}

		private void initializeMemberVariable()
		{
			if( sldOpacity != null )
			{
				mOpacity = sldOpacity.Value / 100.0;
			}
		}

		#endregion



		//////////////////////////////////////////////////
		#region // Preset Crosshair Events & Methods

		private void cbUseCustomCrosshair_Checked( object sender , RoutedEventArgs e )
		{
			// Collapse the preset crosshair list
			gridPresetCrosshair.Visibility = Visibility.Collapsed;

			// Show the settings for custom custom hair
			gridCustomCrosshair.Visibility = Visibility.Visible;
		}

		private void cbPresetCrosshair_SelectionChanged( object sender , SelectionChangedEventArgs e )
		{
			// Get the combobox item
			var combobox = (ComboBox)sender;

			// Create and store the crosshair uri
			mCrosshairUri = new Uri( $"pack://application:,,,/Resources/{combobox.SelectedValue.ToString()}" );

			// Display the crosshair preview image
			Utility.LoadImageWithActualSize( imgCrosshair , mCrosshairUri , mOpacity );

			// Update the crosshair window
			CreateAndUpdateCrosshairWindow();

			// Enable the crosshair button
			btnEnableCrosshair.IsEnabled = true;
		}

		#endregion



		//////////////////////////////////////////////////
		#region // Custom Crosshair Events & Methods

		private void cbUseCustomCrosshair_Unchecked( object sender , RoutedEventArgs e )
		{
			// Show the preset crosshair list
			gridPresetCrosshair.Visibility = Visibility.Visible;

			// Collapse the settings for custom custom hair
			gridCustomCrosshair.Visibility = Visibility.Collapsed;
		}

		private void btnCrosshairSource_Click( object sender , RoutedEventArgs e )
		{
			// Show the open file dialog
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = $"{IMG_FILE}|{BMP_FILE}|{PNG_FILE}|{GIF_FILE}";

			// If a file is selected
			if( openFileDialog.ShowDialog() == true )
			{
				// Change the crosshair textbox text
				tbCrosshairSource.Text = openFileDialog.SafeFileName;

				// Create and store the crosshair uri
				mCrosshairUri = new Uri( openFileDialog.FileName );

				// Display the crosshair preview image
				Utility.LoadImageWithActualSize( imgCrosshair , mCrosshairUri , mOpacity );

				// Check if greater than preview window size
				if( imgCrosshair.Width > MAX_PREVIEW_SIZE || imgCrosshair.Height > MAX_PREVIEW_SIZE )
				{
					// Resize image to max size allow
					imgCrosshair.Width = MAX_PREVIEW_SIZE;
					imgCrosshair.Height = MAX_PREVIEW_SIZE;

					// Change the scaling mode
					RenderOptions.SetBitmapScalingMode( imgCrosshair , BitmapScalingMode.Fant );
				}

				// Update the crosshair window
				CreateAndUpdateCrosshairWindow();

				// Enable the crosshair button
				btnEnableCrosshair.IsEnabled = true;
			}
		}



		#endregion



		//////////////////////////////////////////////////
		#region // Crosshair Position Methods

		#region Offset-X Textbox Methods

		private void OffsetPosX_TextChanged( object sender , TextChangedEventArgs e )
		{
			// Check if value is numeric
			if( Utility.IsTextNumberic( tbOffsetPosX.Text ) )
			{
				// Convert string to int and store it
				mOffsetX = Convert.ToInt32( tbOffsetPosX.Text );

				// Update crosshair position
				CreateAndUpdateCrosshairWindow();
			}
		}

		private void tbOffsetPosX_PreviewKeyDown( object sender , System.Windows.Input.KeyEventArgs e )
		{
			switch( e.Key )
			{
				case System.Windows.Input.Key.Up:
					// Increment the offset
					mOffsetX++;

					// Update the textbox which will update the crosshair
					tbOffsetPosX.Text = mOffsetX.ToString();
					break;
				case System.Windows.Input.Key.Down:
					// Decrement the offset
					mOffsetX--;

					// Update the textbox which will update the crosshair
					tbOffsetPosX.Text = mOffsetX.ToString();
					break;
				case System.Windows.Input.Key.LeftCtrl:
					// Enable offset with mouse control
					bIsControllingOffsetWithMouse = true;
					break;
			}
		}

		private void tbOffsetPosX_PreviewKeyUp( object sender , System.Windows.Input.KeyEventArgs e )
		{
			switch( e.Key )
			{
				case System.Windows.Input.Key.LeftCtrl:
					// Enable offset with mouse control
					bIsControllingOffsetWithMouse = true;
					break;
			}
		}

		private void tbOffsetPosX_PreviewMouseDown( object sender , System.Windows.Input.MouseButtonEventArgs e )
		{
			// Check if controlling crosshair offset
			if( bIsControllingOffsetWithMouse )
			{
				// Capture the mouse
				var ui = (UIElement)sender;
				ui.CaptureMouse();

				// Store the mouse position
				mPreviousPosition = e.MouseDevice.GetPosition( ui );

				// Set offsetting x mode to true
				bIsOffsettingX = true;
			}
		}

		private void tbOffsetPosX_PreviewMouseUp( object sender , System.Windows.Input.MouseButtonEventArgs e )
		{
			// Check if controlling crosshair offset
			if( bIsControllingOffsetWithMouse )
			{
				// Release the mouse
				var ui = (UIElement)sender;
				ui.ReleaseMouseCapture();

				// Release the offseting x mode
				bIsOffsettingX = false;
			}
		}

		private void tbOffsetPosX_PreviewMouseMove( object sender , System.Windows.Input.MouseEventArgs e )
		{
			// Check if is in offsetting x mode
			if( bIsOffsettingX )
			{
				// Get current mouse position
				var ui = (UIElement)sender;
				var currentPosition = e.MouseDevice.GetPosition( ui );

				// Calculate the delta and new offset position
				var deltaY = currentPosition.Y - mPreviousPosition.Y;

				
				// Apply the delta changes base on vertical movement
				mOffsetX += Convert.ToInt32( -deltaY );

				// Update the offset-x textbox which updates the crosshair position
				tbOffsetPosX.Text = mOffsetX.ToString();

				// Update the previous position
				mPreviousPosition = currentPosition;
			}
		}

		private void tbOffsetPosX_PreviewMouseWheel( object sender , System.Windows.Input.MouseWheelEventArgs e )
		{
			// Increment the offset position base on the direction of the mouse scroll
			mOffsetX += (e.Delta > 0) ? 1 : -1;

			// Update offset textbox
			tbOffsetPosX.Text = mOffsetX.ToString();
		}

		#endregion

		#region Offset-X Textbox Methods

		private void OffsetPosY_TextChanged( object sender , TextChangedEventArgs e )
		{
			// Check if value is numeric
			if( Utility.IsTextNumberic( tbOffsetPosY.Text ) )
			{
				// Convert string to int and store it
				mOffsetY = Convert.ToInt32( tbOffsetPosY.Text );

				// Update crosshair position
				CreateAndUpdateCrosshairWindow();
			}
		}

		private void tbOffsetPosY_PreviewKeyDown( object sender , System.Windows.Input.KeyEventArgs e )
		{
			switch( e.Key )
			{
				case System.Windows.Input.Key.Up:
					// Increment the offset
					mOffsetY++;

					// Update the textbox which will update the crosshair
					tbOffsetPosY.Text = mOffsetY.ToString();
					break;
				case System.Windows.Input.Key.Down:
					// Decrement the offset
					mOffsetY--;

					// Update the textbox which will update the crosshair
					tbOffsetPosY.Text = mOffsetY.ToString();
					break;
				case System.Windows.Input.Key.LeftCtrl:
					// Enable offset with mouse control
					bIsControllingOffsetWithMouse = true;
					break;
			}
		}

		private void tbOffsetPosY_PreviewKeyUp( object sender , System.Windows.Input.KeyEventArgs e )
		{
			switch( e.Key )
			{
				case System.Windows.Input.Key.LeftCtrl:
					// Disable offset with mouse control
					bIsControllingOffsetWithMouse = false;
					break;
			}
		}

		private void tbOffsetPosY_PreviewMouseDown( object sender , System.Windows.Input.MouseButtonEventArgs e )
		{
			// Check if controlling crosshair offset
			if( bIsControllingOffsetWithMouse )
			{
				// Capture the mouse
				var ui = (UIElement)sender;
				ui.CaptureMouse();

				// Store the mouse position
				mPreviousPosition = e.MouseDevice.GetPosition( ui );

				// Set offsetting x mode to true
				bIsOffsettingY = true;
			}
		}

		private void tbOffsetPosY_PreviewMouseUp( object sender , System.Windows.Input.MouseButtonEventArgs e )
		{
			// Check if controlling crosshair offset
			if( bIsControllingOffsetWithMouse )
			{
				// Release the mouse
				var ui = (UIElement)sender;
				ui.ReleaseMouseCapture();

				// Release the offseting x mode
				bIsOffsettingY = false;
			}
		}

		private void tbOffsetPosY_PreviewMouseMove( object sender , System.Windows.Input.MouseEventArgs e )
		{
			// Check if is in offsetting x mode
			if( bIsOffsettingY )
			{
				// Get current mouse position
				var ui = (UIElement)sender;
				var currentPosition = e.MouseDevice.GetPosition( ui );

				// Calculate the delta and new offset position
				var deltaY = currentPosition.Y - mPreviousPosition.Y;
				 
				// Apply the delta changes base on vertical movement
				mOffsetY -= Convert.ToInt32( deltaY );

				// Update the offset-x textbox which updates the crosshair position
				tbOffsetPosY.Text = Convert.ToString( mOffsetY );

				// Update the previous position
				mPreviousPosition = currentPosition;
			}
		}

		private void tbOffsetPosY_PreviewMouseWheel( object sender , System.Windows.Input.MouseWheelEventArgs e )
		{
			// Increment the offset position base on the direction of the mouse scroll
			mOffsetY += (e.Delta > 0) ? 1 : -1;

			// Update offset textbox
			tbOffsetPosY.Text = mOffsetY.ToString();
		}

		#endregion

		#endregion



		//////////////////////////////////////////////////
		#region // Crosshair Blending Methods

		private void Slider_ValueChanged( object sender , RoutedPropertyChangedEventArgs<double> e )
		{
			if( tbOpacity != null )
			{
				// Update opacity text
				tbOpacity.Text = sldOpacity.Value.ToString();

				// Update crosshair opacity
				mOpacity = sldOpacity.Value / 100.0;
				imgCrosshair.Opacity = mOpacity;

				// Update crosshair
				CreateAndUpdateCrosshairWindow();
			}
		}

		private void tbOpacity_TextChanged( object sender , TextChangedEventArgs e )
		{
			// Check if input is a valid integer
			if( Utility.IsTextNumberic( tbOpacity.Text ) )
			{
				// Convert input to an int value
				double value = Convert.ToDouble( tbOpacity.Text );

				// If value is less than opacity slider min, value is min
				value = (value < sldOpacity.Minimum) ? sldOpacity.Minimum : value;

				// If value is greater than opacity slider max, value is max
				value = (value > sldOpacity.Maximum) ? sldOpacity.Maximum : value;

				// Update slider
				sldOpacity.Value = value;
			}
		}

		#endregion



		//////////////////////////////////////////////////
		#region // CrosshairWindow Events Methods

		private void btnEnableCrosshair_Click( object sender , RoutedEventArgs e )
		{
			// Create and update the crosshair window
			CreateAndUpdateCrosshairWindow();

			// Check if crosshair is not enabled
			if( !bIsCrosshairEnabled )
			{
				EnableCrosshairWindow();
			}
			else
			{
				DisableCrosshairWindow();
			}
		}

		private void CreateAndUpdateCrosshairWindow()
		{
			// Check if crosshair window has not been created
			if( mCrosshairWindow == null )
			{
				// Create the crosshair window
				mCrosshairWindow = new CrosshairWindow( this );
			}

			// Configure the crosshair
			mCrosshairWindow.CrosshairUri = mCrosshairUri;
			mCrosshairWindow.OffsetX = mOffsetX;
			mCrosshairWindow.OffsetY = -mOffsetY;
			mCrosshairWindow.CrosshairOpacity = mOpacity;

			// Update the crosshair window
			mCrosshairWindow.Update();
		}

		private void EnableCrosshairWindow()
		{
			// Enabled the crosshair
			bIsCrosshairEnabled = true;

			// Change the button text to disable the crosshair
			btnEnableCrosshair.Content = "Disable Crosshair";

			// Show the crosshair window
			mCrosshairWindow.Show();

			// Update the crosshair window position
			mCrosshairWindow.Update();
		}

		private void DisableCrosshairWindow()
		{
			// Disable the crosshair
			bIsCrosshairEnabled = false;

			// Change the button text to enable the crosshair
			btnEnableCrosshair.Content = "Enable Crosshair";

			// Hide the crosshair window
			mCrosshairWindow.Hide();

			// Hide the taskbar
			TaskBar.Show();
		}

		#endregion



		//////////////////////////////////////////////////
		#region // Taskbar Event Methods

		private void btnToogleTaskbar_Click( object sender , RoutedEventArgs e )
		{
			// Toogle the taskbar visibility
			bIsTaskBarVisible = !bIsTaskBarVisible;

			// Check if the taskbar is visible
			if( bIsTaskBarVisible )
			{
				// Show the taskbar
				TaskBar.Show();

				// Change button text to hide taskbar
				btnToogleTaskbar.Content = "Hide the Taskbar";
			}
			else
			{
				// Otherwise hide the taskbar
				TaskBar.Hide();

				// Change button text to show taskbar
				btnToogleTaskbar.Content = "Show the Taskbar";
			}
		}

		#endregion



		//////////////////////////////////////////////////
		#region // IOnCrosshairWindowListener Methods

		public void OnCrosshairWindowClosing()
		{
			// Disable the crosshair
			DisableCrosshairWindow();

			// Set the crosshair window to null
			mCrosshairWindow = null;
		}

		#endregion

		private void Window_Closing( object sender , System.ComponentModel.CancelEventArgs e )
		{
			// Check if crosshair window exist
			if( mCrosshairWindow != null )
			{
				mCrosshairWindow.Close();
			}

			// Show the taskbar if it was hidden
			TaskBar.Show();
		}
	}
}
