﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Desktop_Crosshair
{
	class Utility
	{
		public static bool IsTextNumberic( string text )
		{
			bool result = false;

			// Check if the text is not empty
			if( !String.IsNullOrWhiteSpace( text ) )
			{
				// Create regular expression
				Regex regex = new Regex(@"^([-]?\d+)$");

				// Check regular expression
				result = regex.IsMatch( text );
			}

			return result;
		}

		public static void LoadImageWithActualSize( Image image , Uri uri , double opacity = 1.0 )
		{
			// Create the bitmap
			BitmapImage bitmap = new BitmapImage( uri );

			// Get the system DPI Property
			var dpiXProperty = typeof(SystemParameters).GetProperty("DpiX", BindingFlags.NonPublic | BindingFlags.Static);
			var dpiYProperty = typeof(SystemParameters).GetProperty("Dpi", BindingFlags.NonPublic | BindingFlags.Static);

			// Extra the DPI Value as int
			var dpiX = (int)dpiXProperty.GetValue(null, null);
			var dpiY = (int)dpiYProperty.GetValue(null, null);

			// Calculate the scale
			var scaleX = dpiX / bitmap.DpiX;
			var scaleY = dpiY / bitmap.DpiY;

			// Calculate the image actual dimension
			var actualWidth = Math.Round( bitmap.Width / scaleX );
			var actualHeight = Math.Round( bitmap.Height / scaleY );

			// Set the image and rescale it to actual size
			image.Source = bitmap;
			image.Width = actualWidth;
			image.Height = actualHeight;

			// Set the image opacity
			image.Opacity = opacity;
		}
	}
}
