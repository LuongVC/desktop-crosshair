﻿using Desktop_Crosshair.Classes;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Desktop_Crosshair
{
	/// <summary>
	/// Interaction logic for CrosshairWindow.xaml
	/// </summary>
	public partial class CrosshairWindow: Window
	{
		//////////////////////////////////////////////////
		#region // Member Variables & Properties

		public Uri CrosshairUri { get; set; }
		public int OffsetY { get; set; }
		public int OffsetX { get; set; }
		public double CrosshairOpacity { get; set; }

		private IOnCrosshairWindowListener mCallback = null;

		#endregion



		//////////////////////////////////////////////////
		#region // Constructor & Helper Methods

		public CrosshairWindow( IOnCrosshairWindowListener listener )
		{
			InitializeComponent();

			mCallback = listener;
		}

		#endregion



		//////////////////////////////////////////////////
		#region // Update Methods

		public void Update()
		{
			UpdateCrosshairImage();
			UpdateCrosshairOpacity();
			UpdateCrosshairPosition();
		}

		public void UpdateCrosshairImage()
		{
			// Check if crosshair uri is set
			if( CrosshairUri != null )
			{
				// Update the crosshair				
				Utility.LoadImageWithActualSize( imgCrosshair , CrosshairUri , CrosshairOpacity );

				// Set the window size to match the image size
				Width = imgCrosshair.Width;
				Height = imgCrosshair.Height;
			}
		}

		public void UpdateCrosshairOpacity()
		{
			// Set the image opacity
			imgCrosshair.Opacity = CrosshairOpacity;
		}

		public void UpdateCrosshairPosition()
		{
			// Get system width and height
			double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
			double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;

			// Calculate and center the crosshair window position
			Left = (screenWidth / 2.0) - (imgCrosshair.Width / 2.0) + OffsetX;
			Top = (screenHeight / 2.0) - (imgCrosshair.Height / 2.0) + OffsetY;

			// Set to topmost position
			Topmost = true;
		}

		#endregion

		private void Window_Closing( object sender , System.ComponentModel.CancelEventArgs e )
		{
			if( mCallback != null )
			{
				mCallback.OnCrosshairWindowClosing();
			}
		}
	}
}
