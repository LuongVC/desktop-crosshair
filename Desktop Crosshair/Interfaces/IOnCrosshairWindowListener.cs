﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop_Crosshair
{
	public interface IOnCrosshairWindowListener
	{
		void OnCrosshairWindowClosing();
	}
}
